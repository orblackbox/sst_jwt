import time

from authlib.jose import JsonWebToken

from .JWTException import JWTException
from .KeyStore import key_store
from .models import InsightsToken, UserRole, RoleScope

jwt = JsonWebToken(['RS256'])


class Token:
    def __init__(self, encoded_token: str, key_url: str, is_insights_token: bool = False):
        """
        :param encoded_token: token to decode & validate
        :param key_url: URL that stores public keys of signing server
        """
        self.token = encoded_token
        self.key_url = key_url
        self.claims = self.__get_claims()
        self.insights_token = self.__serialize_insights_token() if is_insights_token else None

    def validate(self, required_claims: dict = None) -> None:
        """
        Checks claims for presence of values. Mainly used for 'aud', 'typ', and 'ip'
        :param required_claims: dict; keys = key in jwt body, value = value to match
        :return: None
        """
        if expiration_time := self.claims.get('exp'):
            if expiration_time < time.time():
                raise JWTException('Expired token')
        if required_claims:
            for key, val in required_claims.items():
                if self.claims.get(key) != val:
                    raise JWTException(f'Validation fail for {key}: {val} != {self.claims.get(key)}')

    """
    PRIVATE METHODS
    """
    def __get_claims(self) -> dict:
        try:
            return jwt.decode(self.token, key=key_store.keys)
        except ValueError:  # key ID not in cache; tries again in case keys have changed
            key_store.refresh(self.key_url)
            return jwt.decode(self.token, key=key_store.keys)

    def __serialize_insights_token(self):
        return InsightsToken(
            sub=self.claims['sub'],
            exp=self.claims['exp'],
            facilityId=self.claims['facilityId'],
            roles={
                rid: UserRole(
                    name=role['name'], scope=RoleScope(**role['scope'])
                ) for rid, role in self.claims['roles'].items()
            }
        )
