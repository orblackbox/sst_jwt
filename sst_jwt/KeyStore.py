import json
import urllib.error
import urllib.parse
import urllib.request


class KeyStore:
    """
    Singleton that stores public keys from each possible source of tokens
    """
    _instance = None
    keys = []

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(KeyStore, cls).__new__(cls)
        return cls._instance

    @classmethod
    def refresh(cls, key_url):
        http_request = urllib.request.Request(key_url, method='GET')
        with urllib.request.urlopen(http_request) as response:
            cls.keys += json.loads(response.read().decode())['keys']


key_store = KeyStore()

