from dataclasses import dataclass
from typing import Dict, List
from uuid import UUID


@dataclass
class RoleScope:
    """UUIDs of scopes that role applies to. E.g. Departments X, Y, Z."""
    h: List[UUID] = ()
    f: List[UUID] = ()
    d: List[UUID] = ()
    r: List[UUID] = ()


@dataclass
class UserRole:
    name: str
    scope: RoleScope


@dataclass
class InsightsToken:
    sub: UUID
    exp: float
    facilityId: UUID
    roles: Dict[UUID, UserRole]  # UUID is role ID

