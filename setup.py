from setuptools import find_packages, setup

setup(
    name='sst_jwt',
    packages=find_packages(include=['sst_jwt']),
    version='0.2.3',
    description='JWT Authentication',
    author='Eric Dong',
    author_email='e.dong@surgicalsafety.com',
    python_requires='>=3.7',
    install_requires=['Authlib>=0.15.0', 'cffi==1.14.5', 'cryptography==3.4.7', 'pycparser==2.20']
)
